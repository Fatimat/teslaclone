# Basic Tesla home page clone

Used  [this](https://www.youtube.com/watch?v=iQ_0Fd_N3Mk) youtube tutorial to do reintroduce myself to react native. 

### Installation 

- Clone repo
- Install dependancies
- Run `yarn start` or `npm start`
- Download expo on your phone to scan QR code to view project
- Or run the project locally