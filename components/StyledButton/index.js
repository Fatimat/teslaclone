import React from 'react';
import { ImageBackground, Pressable, Text, View } from 'react-native';
import styles from './styles';

const StyledButton = ({ type, content, onPress }) => {
  const backgroundColor = type === 'primary' ? '#171a20cc' : '#FFFFFFA6';
  const textColor = type === 'primary' ? '#FFFFFF' : '#171a20';

  return (
    <View style={styles.container}>
      <Pressable
        style={{...styles.button, backgroundColor}} // or {[styles.button, {backgroundColor}]}
        onPress={onPress}
      >
       <Text style={{...styles.text, color: textColor}}>{content}</Text>
      </Pressable>
    </View> 
  );
}

export default StyledButton;